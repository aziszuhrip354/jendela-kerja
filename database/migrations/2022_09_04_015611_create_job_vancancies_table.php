<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobVancanciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_vancancies', function (Blueprint $table) {
            $table->id();
            $table->string("company_name",100);
            $table->string("position")->index();
            $table->text("job_desc");
            $table->text("poster_path");
            $table->text("icon_path");
            $table->text("link_regist");
            $table->text("link_monitoring");
            $table->integer("total_click")->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_vancancies');
    }
}
