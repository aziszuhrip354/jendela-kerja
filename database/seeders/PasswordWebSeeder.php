<?php

namespace Database\Seeders;

use App\Models\Password;
use Illuminate\Database\Seeder;

class PasswordWebSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $checkPasswordExistOrNot = Password::count();
        if(!$checkPasswordExistOrNot) {
            $data = [
                [
                    "site_name" => "user.jk@gmail.com",
                    "password" => bcrypt("useruser")
                ],
                [
                    "site_name" => "admin.jk@gmail.com",
                    "password" => bcrypt("adminadmin")
                ],
            ];
            Password::insert($data);
            echo "\nBerhasil membuat password default\n";
        } else {
            echo "\nPassword was exist!\n";
        }
    }
}
