<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthPasswordMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $getSessionVerify = session("verify_as");
        if(!$getSessionVerify) {
            return redirect()->route("landingpage")->with("error", "Maaf... silahkan Anda sign-in dahulu!");
        }
        return $next($request);
    }
}
