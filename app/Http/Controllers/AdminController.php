<?php

namespace App\Http\Controllers;

use App\Models\JobVancancy;
use App\Models\Password;
use App\Models\Webinar;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function resetPasswordUser()
    {
        return view("admin.ubah-password-user");
    }

    public function resetPasswordAdmin()
    {
        return view("admin.ubah-password-admin");
    }
    
    public function resetPasswordUserPost(Request $request)
    {
        $response = [
            "status" => true,
            "error" => false,
            "data" => $request->all(),
            "code" => 200,
        ];

        $rules = [
            "old_password" => ["required"],
            "new_password" => ["required", "min:8"],
            "confirm_password" => ["required"],
        ];

        $message = [
            "old_password.required" => "Maaf.. silahkan isi password lama user",
            "new_password.required" => "Maaf.. silahkan isi password baru user",
            "new_password.min" => "Maaf.. password baru user minimal 8 digit/huruf",
            "confirm_password.required" => "Maaf.. silahkan isi konfirmasi password baru user",
            
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["status"] = false;
            $response["error"] = true;
            $response["message"] = $validator->errors()->first();
            $response["code"] = 400;
        } else {
            $oldPasswordUser = $request->old_password;
            $newPasswordUser = $request->new_password;
            $confirmPasswordUser = $request->confirm_password;

            $getPassowrdUser = Password::where("site_name", "user.jk@gmail.com")->first();
            if(!(Hash::check($oldPasswordUser, $getPassowrdUser->password))) {
                $response["status"] = false;
                $response["error"] = true;
                $response["message"] = "Password lama Anda salah";
                $response["code"] = 400;
            } else {
                if($newPasswordUser != $confirmPasswordUser) {
                    $response["status"] = false;
                    $response["error"] = true;
                    $response["message"] = "Password baru dan konfirmasi password baru tidak cocok";
                    $response["code"] = 400;
                } else {
                    if($oldPasswordUser == $confirmPasswordUser) {
                        $response["status"] = false;
                        $response["error"] = true;
                        $response["message"] = "Password baru Anda masih sama dengan password lama";
                        $response["code"] = 400;
                    } else {
                        $getPassowrdUser->password = bcrypt($confirmPasswordUser);
                        $getPassowrdUser->update();
                        $response["message"] = "Berhasil mengubah password user";
                    }
                }
            }
            
        }

        return response()->json($response, $response["code"]);
    }

    public function resetPasswordAdminPost(Request $request)
    {
        $response = [
            "status" => true,
            "error" => false,
            "data" => $request->all(),
            "code" => 200,
        ];

        $rules = [
            "old_password" => ["required"],
            "new_password" => ["required", "min:8"],
            "confirm_password" => ["required"],
        ];

        $message = [
            "old_password.required" => "Maaf.. silahkan isi password lama admin",
            "new_password.required" => "Maaf.. silahkan isi password baru admin",
            "new_password.min" => "Maaf.. password baru admin minimal 8 digit/huruf",
            "confirm_password.required" => "Maaf.. silahkan isi konfirmasi password baru admin",
            
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["status"] = false;
            $response["error"] = true;
            $response["message"] = $validator->errors()->first();
            $response["code"] = 400;
        } else {
            $oldPasswordUser = $request->old_password;
            $newPasswordUser = $request->new_password;
            $confirmPasswordUser = $request->confirm_password;

            $getPassowrdAdmin = Password::where("site_name", "admin.jk@gmail.com")->first();
            if(!(Hash::check($oldPasswordUser, $getPassowrdAdmin->password))) {
                $response["status"] = false;
                $response["error"] = true;
                $response["message"] = "Password lama Anda salah";
                $response["code"] = 400;
            } else {
                if($newPasswordUser != $confirmPasswordUser) {
                    $response["status"] = false;
                    $response["error"] = true;
                    $response["message"] = "Password baru dan konfirmasi password baru tidak cocok";
                    $response["code"] = 400;
                } else {
                    if($oldPasswordUser == $confirmPasswordUser) {
                        $response["status"] = false;
                        $response["error"] = true;
                        $response["message"] = "Password baru Anda masih sama dengan password lama";
                        $response["code"] = 400;
                    } else {
                        $getPassowrdAdmin->password = bcrypt($confirmPasswordUser);
                        $getPassowrdAdmin->update();
                        $response["message"] = "Berhasil mengubah password user";
                    }
                }
            }
            
        }

        return response()->json($response, $response["code"]);
    }

    public function webinarPage()
    {
        return view("admin.webinar");
    }

    public function createWebinarPage()
    {
        return view("admin.buat-webinar");
    }

    public function postWebinar(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "error" => false,
        ];

        $regexUrl =  '/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/';

        $rules = [
            "webinarName" => ["required"],
            "desc" => ["required"],
            "linkRegist" => ["required","regex:{$regexUrl}"],
            "linkMonitoring" => ["required","regex:{$regexUrl}"],
            "fileBanner" => ["required"],
            "date" => ["required"],
        ];

        $message = [
            "webinarName.required" => "Nama webinar harus diisi ya!",
            "date.required" => "Tanggal webinar harus diisi ya!",
            "desc.required" => "Deskripsi webinar harus diisi ya!",
            "linkMonitoring.required" => "Link monitoring webinar harus diisi ya!",
            "linkMonitoring.regex" => "Link monitoring webinar harus valid! (http/https)",
            "linkRegist.required" => "Link registrasi webinar harus diisi ya!",
            "linkRegist.regex" => "Link registrasi webinar harus valid! (http/https)",
            "fileBanner.required" => "File banner harus diisi input dulu ya!",
        ];
        $date = date("Y-m-d H:i:s", strtotime($request->date));
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["status"] = false;
            $response["error"] = true;
            $response["message"] = $validator->errors()->first();
            $response["code"] = 400;
        } else {
            $timeNow = Carbon::now();
            if($date < $timeNow) {
                $response["status"] = false;
                $response["error"] = true;
                $response["message"] = "Maaf tanggal webinar tidak boleh waktu yang telah lewat!";
                $response["code"] = 400;
            } else {
                $webinarName = $request->webinarName;
                $description = $request->desc;
                $linkMonitoring = $request->linkMonitoring;
                $linkRegist = $request->linkRegist;
                $fileBanner =$request->file("fileBanner");
                
                $createWebinar = new Webinar();
                // CODE STORING FILE 
                $filename = "banner-".str_replace(":", "", str_replace(" ", "", Carbon::now())) . "." . $fileBanner->extension();  
                $path = '/storage/banner/' . $filename;
                $fileBanner->storeAs('/banner/', $filename, 'public');
                $createWebinar->banner_path = $path;
                // END CODE STORING FILE 
                $createWebinar->webinar_name = $webinarName;
                $createWebinar->deskripsi = $description;
                $createWebinar->link_regist = $linkRegist;
                $createWebinar->link_monitoring = $linkMonitoring;
                $createWebinar->be_held = $date;
                $createWebinar->save();
                $response["message"] = "Berhasil menyimpan informasi webinar";
            }
        }

        return response()->json($response, $response["code"]);
    }

    public function listWebinar(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "error" => false,
            "data" => $request->all()
        ];

        $getPaginateWebinar = Webinar::orderBy("id","DESC")->paginate(6);
        $response["data"] = $getPaginateWebinar;
        
        return response($response, $response["code"]);
    }

    public function deleteWebinar(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "error" => false,
            "data" => $request->all(),
        ];
        $findWebinarById = Webinar::find(base64_decode($request->codeIdWebinar));
        if(!$findWebinarById) {
            $response["code"] = 500;
            $response["status"] = false;
            $response["error"] = true;
            $response["message"] = "Webinar not valid!";
        } else {
            $pathBanner = storage_path("app/public/".str_replace("/storage/","",$findWebinarById->banner_path));
            $checkFileBanner = file_exists($pathBanner);
            if($checkFileBanner) {
                unlink($pathBanner);
            }            
            $findWebinarById->delete();
            $response["message"] = "Berhasil menghapus webinar";
        }

        return response()->json($response, $response["code"]);
    }

    public function webinarEditPage($codeWebinar)
    {
        $findWebinar = Webinar::find(base64_decode($codeWebinar));
        if(!$findWebinar) {
            abort(404);
        } else {
            $dataArr = [];
            $dataArr[] = "findWebinar";
        }
        return view("admin.edit-webinar", compact($dataArr));
    }

    public function updateWebinar(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "error" => false,
        ];
        $idWebinar = base64_decode($request->codeIdWebinar);

        $regexUrl =  '/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/';

        $rules = [
            "webinarName" => ["required"],
            "desc" => ["required"],
            "linkRegist" => ["required","regex:{$regexUrl}"],
            "linkMonitoring" => ["required","regex:{$regexUrl}"],
            "date" => ["required"],
        ];

        $message = [
            "webinarName.required" => "Nama webinar harus diisi ya!",
            "desc.required" => "Deskripsi webinar harus diisi ya!",
            "date.required" => "Tanggal webinar harus diisi ya!",
            "linkMonitoring.required" => "Link monitoring webinar harus diisi ya!",
            "linkMonitoring.regex" => "Link monitoring webinar harus valid! (http/https)",
            "linkRegist.required" => "Link registrasi webinar harus diisi ya!",
            "linkRegist.regex" => "Link registrasi webinar harus valid! (http/https)",
        ];

        $date = date("Y-m-d H:i:s", strtotime($request->date));
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["status"] = false;
            $response["error"] = true;
            $response["message"] = $validator->errors()->first();
            $response["code"] = 400;
        } else {
            $timeNow = Carbon::now();
            $timeNow = Carbon::now();
            if($date < $timeNow) {
                $response["status"] = false;
                $response["error"] = true;
                $response["message"] = "Maaf tanggal webinar tidak boleh waktu yang telah lewat!";
                $response["code"] = 400;
            } else {
                $webinarName = $request->webinarName;
                $description = $request->desc;
                $linkMonitoring = $request->linkMonitoring;
                $linkRegist = $request->linkRegist;
                
                $findWebinar = Webinar::find($idWebinar);
                if(!$findWebinar) {
                    $response["status"] = false;
                    $response["error"] = true;
                    $response["message"] = "Webinar tidak valid";
                    $response["code"] = 400;
                } else {
                    $fileBanner = $request->file("fileBanner");
                    // CODE TO DELETE OLD FILE
                    if($fileBanner) {
                        $pathBanner = storage_path("app/public/".str_replace("/storage/","",$findWebinar->banner_path));
                        $checkFileBanner = file_exists($pathBanner);
                        if($checkFileBanner) {
                            unlink($pathBanner);
                        }   
                        // END CODE TO DELETE OLD FILE
                        // CODE STORING FILE 
                        $filename = "banner-".str_replace(":", "", str_replace(" ", "", Carbon::now())) . "." . $fileBanner->extension();  
                        $path = '/storage/banner/' . $filename;
                        $fileBanner->storeAs('/banner/', $filename, 'public');
                        $findWebinar->banner_path = $path;
                        // END CODE STORING FILE 
                    }
                    $findWebinar->webinar_name = $webinarName;
                    $findWebinar->deskripsi = $description;
                    $findWebinar->link_regist = $linkRegist;
                    $findWebinar->link_monitoring = $linkMonitoring;
                    $findWebinar->be_held = $date;
                    $findWebinar->save();
                    $response["message"] = "Berhasil menyimpan informasi webinar";
                }
            }
        }

        return response()->json($response, $response["code"]);
    }

    public function jobPage()
    {
        return view("admin.loker");
    }

    public function createJobPage()
    {
        return view("admin.buat-loker");
    }

    public function postJob(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "error" => false,
        ];

        $regexUrl =  '/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/';

        $rules = [
            "jobName" => ["required"],
            "company_name" => ["required"],
            "desc" => ["required"],
            "linkRegist" => ["required","regex:{$regexUrl}"],
            "linkMonitoring" => ["required","regex:{$regexUrl}"],
            "fileLogo" => ["required"],
            "filePoster" => ["required"],
        ];

        $message = [
            "jobName.required" => "Nama loker (posisi) harus diisi ya!",
            "company_name.required" => "Nama perusahaan harus diisi ya!",
            "desc.required" => "Deskripsi loker harus diisi ya!",
            "linkMonitoring.required" => "Link monitoring loker harus diisi ya!",
            "linkMonitoring.regex" => "Link monitoring loker harus valid! (http/https)",
            "linkRegist.required" => "Link registrasi loker harus diisi ya!",
            "linkRegist.regex" => "Link registrasi loker harus valid! (http/https)",
            "fileLogo.required" => "File logo perusahaan harus diisi input dulu ya!",
            "filePoster.required" => "File poster harus diisi input dulu ya!",
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["status"] = false;
            $response["error"] = true;
            $response["message"] = $validator->errors()->first();
            $response["code"] = 400;
        } else {
            $jobName = $request->jobName;
            $description = $request->desc;
            $linkMonitoring = $request->linkMonitoring;
            $companyName = $request->company_name;
            $linkRegist = $request->linkRegist;
            $fileLogo =$request->file("fileLogo");
            $filePoster =$request->file("filePoster");
            
            $createJob = new JobVancancy();
            // CODE STORING FILE 
            $filename = "fileLogo-".str_replace(":", "", str_replace(" ", "", Carbon::now())) . "." . $fileLogo->extension();  
            $path = '/storage/job/' . $filename;
            $fileLogo->storeAs('/job/', $filename, 'public');
            $createJob->icon_path = $path;

            $filename = "filePoster-".str_replace(":", "", str_replace(" ", "", Carbon::now())) . "." . $filePoster->extension();  
            $path = '/storage/job/' . $filename;
            $filePoster->storeAs('/job/', $filename, 'public');
            $createJob->poster_path = $path;
            // END CODE STORING FILE 
            $createJob->position = $jobName;
            $createJob->job_desc = $description;
            $createJob->link_regist = $linkRegist;
            $createJob->link_monitoring = $linkMonitoring;
            $createJob->company_name = $companyName;
            $createJob->save();
            $response["message"] = "Berhasil menyimpan informasi loker baru";
        }

        return response()->json($response, $response["code"]);
    }

    public function listJob(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "error" => false,
            "data" => $request->all()
        ];

        $getPaginateWebinar = JobVancancy::orderBy("id","DESC")->paginate(6);
        $response["data"] = $getPaginateWebinar;
        
        return response($response, $response["code"]);
    }

    public function deleteJob(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "error" => false,
            "data" => $request->all(),
        ];
        $findJobById = JobVancancy::find(base64_decode($request->codeIdJob));
        if(!$findJobById) {
            $response["code"] = 500;
            $response["status"] = false;
            $response["error"] = true;
            $response["message"] = "Job not valid!";
        } else {
            $pathLogo = storage_path("app/public/".str_replace("/storage/","",$findJobById->icon_path));
            $checkFileLogo = file_exists($pathLogo);
            if($checkFileLogo) {
                unlink($pathLogo);
            }            
            $pathPoster = storage_path("app/public/".str_replace("/storage/","",$findJobById->poster_path));
            $checkFilePoster = file_exists($pathPoster);
            if($checkFilePoster) {
                unlink($pathPoster);
            }    
            $findJobById->delete();
            $response["message"] = "Berhasil menghapus job";
        }

        return response()->json($response, $response["code"]);
    }

    public function jobEditPage($codeWebinar)
    {
        $findJob = JobVancancy::find(base64_decode($codeWebinar));
        if(!$findJob) {
            abort(404);
        } else {
            $dataArr = [];
            $dataArr[] = "findJob";
        }
        return view("admin.edit-loker", compact($dataArr));
    }

    public function updateJob(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "error" => false,
        ];
        $idJob = base64_decode($request->codeIdJob);

        $regexUrl =  '/^((?:https?\:\/\/|www\.)(?:[-a-z0-9]+\.)*[-a-z0-9]+.*)$/';

        $rules = [
            "jobName" => ["required"],
            "company_name" => ["required"],
            "desc" => ["required"],
            "linkRegist" => ["required","regex:{$regexUrl}"],
            "linkMonitoring" => ["required","regex:{$regexUrl}"],
        ];

        $message = [
            "jobName.required" => "Nama loker (posisi) harus diisi ya!",
            "company_name.required" => "Nama peruhaan harus diisi ya!",
            "desc.required" => "Deskripsi loker harus diisi ya!",
            "linkMonitoring.required" => "Link monitoring loker harus diisi ya!",
            "linkMonitoring.regex" => "Link monitoring loker harus valid! (http/https)",
            "linkRegist.required" => "Link registrasi loker harus diisi ya!",
            "linkRegist.regex" => "Link registrasi loker harus valid! (http/https)",
        ];

        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()) {
            $response["status"] = false;
            $response["error"] = true;
            $response["message"] = $validator->errors()->first();
            $response["code"] = 400;
        } else {
            $jobName = $request->jobName;
            $description = $request->desc;
            $linkMonitoring = $request->linkMonitoring;
            $linkRegist = $request->linkRegist;
            $companyName = $request->company_name;
            
            $findJob = JobVancancy::find($idJob);
            if(!$findJob) {
                $response["status"] = false;
                $response["error"] = true;
                $response["message"] = "Webinar tidak valid";
                $response["code"] = 400;
            } else {
                $fileLogo = $request->file("fileLogo");
                $filePoster = $request->file("filePoster");
                // CODE TO DELETE OLD FILE
                if($fileLogo) {
                    $pathLogo = storage_path("app/public/".str_replace("/storage/","",$findJob->icon_path));
                    $checkFileLogo = file_exists($pathLogo);
                    if($checkFileLogo) {
                        unlink($pathLogo);
                    }   
                    // END CODE TO DELETE OLD FILE
                    // CODE STORING FILE 
                    $filename = "fileLogo-".str_replace(":", "", str_replace(" ", "", Carbon::now())) . "." . $fileLogo->extension();  
                    $path = '/storage/job/' . $filename;
                    $fileLogo->storeAs('/job/', $filename, 'public');
                    $findJob->icon_path = $path;
                    // END CODE STORING FILE 
                }
                if($filePoster) {
                    $pathPoster = storage_path("app/public/".str_replace("/storage/","",$findJob->poster_path));
                    $checkFilePoster = file_exists($pathPoster);
                    if($checkFilePoster) {
                        unlink($pathPoster);
                    }   
                    // END CODE TO DELETE OLD FILE
                    // CODE STORING FILE 
                    $filename = "filePoster-".str_replace(":", "", str_replace(" ", "", Carbon::now())) . "." . $filePoster->extension();  
                    $path = '/storage/job/' . $filename;
                    $filePoster->storeAs('/job/', $filename, 'public');
                    $findJob->poster_path = $path;
                    // END CODE STORING FILE 
                }
                $findJob->position = $jobName;
                $findJob->job_desc = $description;
                $findJob->company_name = $companyName;
                $findJob->link_regist = $linkRegist;
                $findJob->link_monitoring = $linkMonitoring;
                $findJob->save();
                $response["message"] = "Berhasil menyimpan informasi loker";
            }
        }

        return response()->json($response, $response["code"]);
    }

}
