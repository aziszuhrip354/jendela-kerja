<?php

namespace App\Http\Controllers;

use App\Models\Password;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function signIn()
    {
        return view("auth.signin");
    }

    public function sigInAdmin()
    {
        return view("auth.signin-admin");
    }

    public function signInPost(Request $request)
    {
        $response = [
            "status" => true,
            "error" => false,
            "data" => $request->all()
        ];

        $password = $request->password;
        if(!$password) {
            $response["status"] = false;
            $response['error'] = true;
            $response['message'] = "Maaf silahkan masukan password dahulu";
        } else {
            $email = $request->email;
            $getPassowrdUser = Password::where("site_name", $email)->first();
            if(!$getPassowrdUser) {
                $response["status"] = false;
                $response['error'] = true;
                $response['message'] = "Maaf email tidak ditemukan";
            } else {
                if(Hash::check($password, $getPassowrdUser->password)) {
                    if($getPassowrdUser->site_name == "admin.jk@gmail.com") {
                        $response['url_valid'] = route('admin.webinar.page');
                        session(["verify_as" => "admin"]);
                        $response['message'] = "Berhasil sign-in sebagai admin";
                    } else if($getPassowrdUser->site_name == "user.jk@gmail.com") {
                        session(["verify_as" => "user"]);
                        $response['message'] = "Berhasil sign-in sebagai user";
                        $response['url_valid'] = route('landingpage');
                    }
                } else {
                    $response["status"] = false;
                    $response['error'] = true;
                    $response['message'] = "Maaf... password salah!";
                }
            }
        }

        return response()->json($response);
    }

    public function logout()
    {
        session()->flush();
        session()->regenerate();

        return redirect()->route("signIn");
    }
}
