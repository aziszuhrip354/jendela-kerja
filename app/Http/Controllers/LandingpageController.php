<?php

namespace App\Http\Controllers;

use App\Models\JobVancancy;
use App\Models\Webinar;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LandingpageController extends Controller
{
    public function landingpage()
    {
        return view("landingpage");
    }

    public function getDataWebinar($skipped, $limit)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "error" => false,
            "data" => [$skipped, $limit]
        ];

        $timeNow = Carbon::now();
        $getDataWebinar = Webinar::orderBy("be_held","ASC")
                          ->where("be_held",">", $timeNow)
                          ->skip($skipped)
                          ->take($limit)
                          ->get(); 
        $response["data"] = $getDataWebinar;
        return response()->json($response, $response['code']);
    }

    public function linkRegistJob($codeId)
    {
        $findJob = JobVancancy::find(base64_decode($codeId));
        if(!$findJob) {
            abort(404);
        } else {
            $findJob->total_click++;
            $findJob->update();
        }

        return redirect()->to($findJob->link_regist);
    }

    public function linkRegist($codeId)
    {
        $findWebinar = Webinar::find(base64_decode($codeId));
        if(!$findWebinar) {
            abort(404);
        } else {
            $findWebinar->total_click++;
            $findWebinar->update();
        }

        return redirect()->to($findWebinar->link_regist);
    }

    public function getJobPagination(Request $request)
    {
        $response = [
            "code" => 200,
            "status" => true,
            "error" => false,
            "data" => $request->all()
        ];
        
        $getPaginateWebinar = JobVancancy::select("*");

        $search = $request->search;
        if($search) {
            // $query->whereRaw('LOWER(`newsTitle`) LIKE ? ',[trim(strtolower($newsTitle)).'%']);
            $getPaginateWebinar->whereRaw('LOWER(`position`) LIKE ? ',['%'.strtolower($search).'%']);
            $getPaginateWebinar->orWhereRaw('LOWER(`company_name`) LIKE ? ',['%'.strtolower($search).'%']);
            // $query->whereRaw('LOWER(`newsTitle`) LIKE ? ',[trim(strtolower($newsTitle)).'%']);
            // $getPaginateWebinar->where("lower(`position`)", "like", "%".strtolower($search)."%");
            // $getPaginateWebinar->orWhere("lower(`company_name`)", "like", "%".strtolower($search)."%");
        }

        $filter = $request->filter;
        switch ($filter) {
            case 'latest':
               $getPaginateWebinar->orderBy("created_at","DESC");
                break;
            case 'oldest':
               $getPaginateWebinar->orderBy("created_at","ASC");
                break;
            case 'most_like':
               $getPaginateWebinar->orderBy("total_click","DESC");
                break;
            case 'not_most_like':
                $getPaginateWebinar->orderBy("total_click","ASC");
                    break;
            default:
               $getPaginateWebinar->orderBy("created_at","DESC");
                break;
        }
        
        $data = $getPaginateWebinar->paginate(6);

        $response["data"] = $data;
        
        return response($response, $response["code"]);
    }
}
