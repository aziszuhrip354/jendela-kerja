<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jendela Kerja | @yield('title')</title>
    <link href="{{asset("css/bootstrap5-2.css")}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset("css/style.css")}}">
  </head>
  <body style="overflow-x: hidden!important">
    <nav class="navbar navbar-expand-lg hide-in-web">
        <div class="container navbar-content">
          <a class="navbar-brand txt-secondary fw-bold" href="#beranda">Carexpo</a>
          <button class="border-none btnHamburger" id="btnHamburger">
          <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent" data-button="false">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0" id="navbarMobile">
              <li class="nav-item">
                <a class="nav-link" href="{{route('landingpage')}}#beranda">Beranda</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('landingpage')}}#webinar">Webinar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('landingpage')}}#tentang">Tentang</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('landingpage')}}#loker">Loker</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="{{route('landingpage')}}#kontak">Kontak</a>
              </li>
              @if(!session("verify_as"))
                <li class="nav-item">
                  <a class="nav-link" href="{{route('signIn')}}">Masuk</a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link" href="javascript:void(0)" onclick="logOut('logoutForm')">Logout</a>
                  <form action="{{route('logout')}}" id="logoutForm" method="POST">
                      @csrf
                  </form>
                </li>
              @endif
            </ul>
            <div class="text-center t-small mt-3 d-none footer-navbar-mobile">
              © 2020 Tickitz. All Rights Reserved.
            </div>
            <div class="d-flex d-none-in-mobile" role="search">
                @if (!session("verify_as"))
                    <a class="btn-white" href="{{route('signIn')}}">Masuk</a>
                @else
                    <a class="btn-white" href="javascript:void(0)" onclick="logOut('logoutForm')">Logout</a>
                    <form action="{{route('logout')}}" id="logoutForm" method="POST">
                        @csrf
                    </form>
                @endif
            </div>
          </div>
        </div>
      </nav>
    <div  id="containerForm">
        <div class="vh-100 over-yx-auto " style="overflow-x: hidden!important" id="app">
            <div class="row h-100" >
                <div class="col-md-5 h-100" >
                    <div class="w-100 h-100" >
                        <img class="w-100 h-100" src="@yield("banner")" alt="">
                    </div>
                </div>
                <div class="col-md-7 d-flex justify-content-center align-items-center">  
                    <div class="container-fluid px-7rem my-4">
                        @yield("form")
                    </div>
                </div>
            </div>
        </div>
        <div class="container-loading vh-100 w-100" id="containerLoading">
            <div class="load-wrapp">
                <div class="load-3 flexs">
                  <h4 class="fw-bold-600 text-center text-white">Loading...</h4>
                  <div class="d-flex justify-content-center">
                      <div class="line"></div>
                      <div class="line"></div>
                      <div class="line"></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="p-4 vh-100 d-flex justify-content-center align-items-center hide-in-web">
        <div class="row">
            <div class="col-md-12">
                <h2 class="fw-bold-600 txt-primary">Selamat datang di</h2>
            </div>
            <div class="col-md-12">
                <h2 class="txt-secondary fw-bold-600">Jendela Kerja!</h2>
            </div>
            <div class="col-md-12 ">
                <p class="fw-bold slim-font200" style="color: gray; font-size: 13px">Silahkan masukan password untuk mengakses <br>
                lebih lanjut</p>
            </div>
            <div class="col-md-12">
                <form method="POST" action="javascript:void(0)" id="formSignIn">
                    <input type="hidden" name="url_hashed2" value="{{base64_encode(route("signIn.post"))}}">
                    <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label text-muted fw-bold-600">Email</label>
                        <input type="email" name="email2" class="form-control input-primay" placeholder="Masukan email...">
                      </div>
                    <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label text-muted fw-bold-600">Password</label>
                      <input type="password" name="password2" class="form-control input-primay" placeholder="Masukan password..." id="exampleInputPassword1">
                    </div>
                    <button class="buttons text-white bg-main w-100" onclick="signIn2(this)">Masuk</button>
                  </form>
            </div>
        </div>
    </div>
    <script src="{{asset("js/jquery3-6.js")}}"></script>
    <script src="{{asset("js/js-bootstrap5-2.js")}}"></script>
    <script src="{{asset("js/swal2.js")}}"></script>
    <script>
        $("#btnHamburger").on("click", function() {
      let dataButton = $("#navbarSupportedContent").attr("data-button");
      if(dataButton == "false") {
        $("#navbarSupportedContent").attr("data-button", "true")
        $("#navbarSupportedContent").addClass("show");
      } else {
        $("#navbarSupportedContent").attr("data-button", "false")
        $("#navbarSupportedContent").removeClass("show");
      }
    });
        @if(session("error"))
            Swal.fire({
                icon: 'error',
                title: "Pesan!",
                text: "{{session('error')}}",
                showConfirmButton: false,
                timer: 3000
            });
        @endif

        $.ajaxSetup({
            headers : {
                'X-CSRF-TOKEN' : '{{csrf_token()}}'
            }
        })

        var $loading = $('#containerLoading').hide();
        $(document)
            .ajaxStart(function () {
                $loading.show();
            })
            .ajaxStop(function () {
                $loading.hide();
        });

        const preventDefault = (id) => {
            $("#"+id).on("submit", e => {
                e.preventDefault();
            });
        }
        const signIn2 = (self) => {
            let url = $("[name='url_hashed2']").val();
            url = atob(url);
            let password = $("[name='password2']").val();
            let email = $("[name='email2']").val()
            $.ajax({
                url: url,
                dataType: "json",
                type: "POST",
                async: true,
                data: {password, email},
                success: function (data) {
                    if(data.status) {
                        Swal.fire({
                            icon: 'success',
                            title: "Pesan!",
                            text: data.message,
                            showConfirmButton: false,
                            timer: 3000
                        });
                        setTimeout(() => {
                            location.href = data.url_valid;
                        }, 3000);
                    } else {
                        Swal.fire({
                            icon: 'warning',
                            title: "Pesan!",
                            text: data.message,
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (xhr, exception) {
                    Swal.fire({
                        icon: 'error',
                        title: "Pesan!",
                        text: "Something error!",
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            }); 
            $("[name='password']").val("");
            preventDefault("formSignIn");
        }
        const signIn = (self) => {
            let url = $("[name='url_hashed']").val();
            url = atob(url);
            let password = $("[name='password']").val();
            let email = $("[name='email']").val()
            $.ajax({
                url: url,
                dataType: "json",
                type: "POST",
                async: true,
                data: {password, email},
                success: function (data) {
                    if(data.status) {
                        Swal.fire({
                            icon: 'success',
                            title: "Pesan!",
                            text: data.message,
                            showConfirmButton: false,
                            timer: 3000
                        });
                        setTimeout(() => {
                            location.href = data.url_valid;
                        }, 3000);
                    } else {
                        Swal.fire({
                            icon: 'warning',
                            title: "Pesan!",
                            text: data.message,
                            showConfirmButton: false,
                            timer: 3000
                        });
                    }
                },
                error: function (xhr, exception) {
                    Swal.fire({
                        icon: 'error',
                        title: "Pesan!",
                        text: "Something error!",
                        showConfirmButton: false,
                        timer: 3000
                    });
                }
            }); 
            $("[name='password']").val("");
            preventDefault("formSignIn");
        }
</script>
  </body>
</html>
