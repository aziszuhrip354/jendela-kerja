@extends('auth.template')

@section('title')
    Sign In 
@endsection

@section('banner')
{{asset("assets/banner-signin.png")}}
@endsection

@section('form')
<div class="row">
    <div class="col-md-12">
        <h2 class="fw-bold-600 txt-primary">Selamat datang di</h2>
    </div>
    <div class="col-md-12">
        <h2 class="txt-secondary fw-bold-600">Jendela Kerja!</h2>
    </div>
    <div class="col-md-12">
        <p class="txt-muted slim-font200">Silahkan masukan password untuk mengakses <br>
        lebih lanjut</p>
    </div>
    <div class="col-md-12">
        <form method="POST" id="formSignIn">
            <input type="hidden" name="url_hashed" value="{{base64_encode(route("signIn.post"))}}">
            <div class="mb-3">
                <label for="exampleInputPassword1" class="form-label text-muted fw-bold-600">Email</label>
                <input type="email" name="email" class="form-control input-primay" placeholder="Masukan email...">
              </div>
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label text-muted fw-bold-600">Password</label>
              <input type="password" name="password" class="form-control input-primay" placeholder="Masukan password..." id="exampleInputPassword1">
            </div>
            <button class="buttons text-white bg-main w-100" onclick="signIn(this)">Masuk</button>
          </form>
    </div>
</div>
@endsection