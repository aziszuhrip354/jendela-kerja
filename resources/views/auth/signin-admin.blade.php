@extends('auth.template')

@section('title')
    Sign In Admin
@endsection

@section('banner')
{{asset("assets/banner-signin.png")}}
@endsection

@section('form')
<div class="row">
    <div class="col-md-12">
        <h2 class="fw-bold-600 txt-primary">Selamat datang di</h2>
    </div>
    <div class="col-md-12">
        <h2 class="fw-bold-600 txt-primary">Halaman Admin</h2>
    </div>
    <div class="col-md-12">
        <h2 class="txt-secondary fw-bold-600">Jendela Kerja!</h2>
    </div>
    <div class="col-md-12">
        <p class="txt-muted slim-font200">Silahkan masukan password untuk mengakses <br>
        lebih lanjut</p>
    </div>
    <div class="col-md-12">
        <form>
            <div class="mb-3">
              <label for="exampleInputPassword1" class="form-label text-muted fw-bold-600">Password</label>
              <input type="password" class="form-control input-primay" placeholder="Masukan password" id="exampleInputPassword1">
            </div>
            <button type="submit" class="buttons text-white bg-main w-100">Masuk</button>
          </form>
    </div>
</div>
@endsection