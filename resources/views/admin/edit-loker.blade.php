@extends('admin.template')

@section("title")
Edit Loker
@endsection

@section('css')
<link rel="stylesheet" href="{{asset("css/filepond.css")}}"/>
<link rel="stylesheet" href="{{asset("css/filepond-image.css")}}"/>
@endsection


@section('breadcrumb')
<li class="breadcrumb-item text-sm"><a class="opacity-5 text-white" href="javascript:;">Dashboard</a></li>
<li class="breadcrumb-item text-sm text-white active" aria-current="page">Edit Loker</li>    
@endsection

@section('content')
<div class="card-header pb-0">
  <h6>EDIT LOKER : <span class="font-weight-bold text-white bg-primary px-2 rounded">{{$findJob->position}}</span></h6>
  
</div>
    <div class="card-body px-0 pt-0 pb-2" class="p-4">    
      <form class="p-4" enctype="multipart/form-data">
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Nama Perusaan</label>
            <input type="text" name="company_name" value="{{$findJob->company_name}}" class="form-control">
          </div>
          <div class="mb-3">
            <label for="" class="">Upload Logo Perusaan <span class="text-muted font-italic">(Jika ingin ganti logo silahkan upload kembali)</span></label>
            <input type="file" id="file_logo" class="input_filepond">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Nama Posisi</label>
            <input type="text" name="job_name" value="{{$findJob->position}}" class="form-control">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Deskripsi Webinar</label>
            <textarea name="desc" id="" cols="20" rows="10" class="form-control">{{$findJob->job_desc}}</textarea>
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Link Pendaftaran</label>
            <input type="text" value="{{$findJob->link_regist}}"  name="link_regist" class="form-control">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Link Monitoring</label>
            <input type="text" value="{{$findJob->link_monitoring}}" name="link_monitoring" class="form-control">
          </div>
          <div class="mb-3">
            <label for="" class="">Upload Poster <span class="text-muted font-italic">(Jika ingin ganti poster silahkan upload kembali)</span></label>
            <input type="file" class="input_filepond" id="file_poster">
          </div>
          <button type="button" class="btn btn-primary" onclick="updateWebinar();">Simpan</button>
        </form>
  </div>
</div>
@endsection

@section('js')
<script src="{{asset("js/filepond.min.js")}}"></script>
<script src="{{asset("js/filepond-image.js")}}"></script>
<script src="{{asset("js/filepond-jquery.js")}}"></script>
<script src="{{asset("js/filepond-encoded.js")}}"></script>
<script src="{{asset("js/filepond-validation-image.js")}}"></script>
<script src="https://cdn.jsdelivr.net/npm/@tinymce/tinymce-jquery@2/dist/tinymce-jquery.min.js"></script>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>

<script>
  $('[name="desc"]').tinymce({ height: 500});
  FilePond.registerPlugin(FilePondPluginFileValidateType);
  FilePond.registerPlugin(FilePondPluginImagePreview);
  $('.input_filepond').filepond();
  // Turn input element into a pond with configuration options
  $('.input_filepond').filepond({
      allowMultiple: false,
      allowImagePreview: true,
      allowFileTypeValidation: true,
      allowProcess: true,
      allowFileEncode: true,
  });
  const filepond = FilePond.create(document.querySelector("#file_logo"), {
    onprocessfile: (err, fileItem) => {
      console.log(err, fileItem.file);
    },
    instantUpload: true,
    acceptedFileTypes: ['image/png','image/jpg','image/jpeg'],
    labelFileTypeNotAllowed: "File yang di bolehkan hanya .jpg/.png/.jpeg",

    fileValidateTypeDetectType: (source, type) =>
        new Promise((resolve, reject) => {
            // Do custom type detection here and return with promise
            resolve(type);
        }),
  });

  const filepond2 = FilePond.create(document.querySelector("#file_poster"), {
    onprocessfile: (err, fileItem) => {
      console.log(err, fileItem.file);
    },
    instantUpload: true,
    acceptedFileTypes: ['image/png','image/jpg','image/jpeg'],
    labelFileTypeNotAllowed: "File yang di bolehkan hanya .jpg/.png/.jpeg",

    fileValidateTypeDetectType: (source, type) =>
        new Promise((resolve, reject) => {
            // Do custom type detection here and return with promise
            resolve(type);
        }),
    });
  
  const updateWebinar = () => {
    let dataForm = new FormData();
    let jobName = $("[name='job_name']").val();
    let desc = $("[name='desc']").val();
    let linkRegist = $("[name='link_regist']").val();
    let linkMonitoring = $("[name='link_monitoring']").val();
    let companyName = $("[name='company_name']").val();
    console.log(desc, jobName, linkRegist, linkMonitoring);
    let fileLogo = filepond.getFile();
    let filePoster = filepond2.getFile();
    dataForm.append("jobName", jobName == undefined ? "" : jobName );
    dataForm.append("desc", desc == undefined ? "" : desc );
    dataForm.append("linkRegist", linkRegist == undefined ? "" : linkRegist );
    dataForm.append("linkMonitoring", linkMonitoring == undefined ? "" : linkMonitoring );
    dataForm.append("company_name", companyName == undefined ? "" : companyName );
    dataForm.append("codeIdJob", "{{base64_encode($findJob->id)}}");

    try {
      dataForm.append("fileLogo", fileLogo.file);  
      dataForm.append("filePoster", filePoster.file);
    } catch (error) {
      
    }

    $.ajax({
        url: "{{ route('admin.create.job.edit.post') }}",
        dataType: "json",
        type: "POST",
        async: true,
        processData: false,
        contentType: false,
        data: dataForm,
        success: function(data) {
          Swal.fire({
              icon: 'success',
              title: "Pesan!",
              text: data.message,
              showConfirmButton: false,
              timer: 3000
          });
          setTimeout(() => {
            location.href = "{{route('admin.job.page')}}";
          }, 3000);
        },
        error: function(xhr, exception) {
          let error = xhr.responseJSON;
          Swal.fire({
              icon: 'error',
              title: "Pesan!",
              text: error.message,
              showConfirmButton: false,
              timer: 3000
          });
        }
    });

  }
</script>
@endsection