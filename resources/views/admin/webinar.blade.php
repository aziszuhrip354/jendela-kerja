@extends('admin.template')

@section("title")
Webinar Page
@endsection

@section('css')
    
@endsection

@section('breadcrumb')
<li class="breadcrumb-item text-sm"><a class="opacity-5 text-white" href="javascript:;">Dashboard</a></li>
<li class="breadcrumb-item text-sm text-white active" aria-current="page">Webinar Page</li>    
@endsection


@section('content')
<input type="text" id="clipboard_input" class="d-none">
<div class="card-header pb-0">
    <h6 style="display: flex;align-items: center;justify-content: space-between;">DAFTAR WEBINAR <a href="{{route('admin.create.webinar.page')}}" class="btn btn-primary m-0">
        WEBINAR BARU
    </a></h6>
    
  </div>
  {{-- <button type="button" class="btn btn-primary m-0"  data-bs-toggle="modal" data-bs-target="#modalTokenBaru">
    WEBINAR BARU
  </button> --}}
  <div class="card-body px-0 pt-0 pb-2 scrolling">
    <div class="container-fluid my-4">
      <div class="flexing-down" id="containerWerbinar">
        <div class="col">
          <div class="card" style="width: 18rem;">
            {{-- <img src="{{}}" class="card-img-top" alt="..."> --}}
            {{-- <div class="card-body" style="position: relative;">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
              <div class="row">
                <a href="#" class="btn btn-primary col mx-1">Detail</a>
              </div>
            </div> --}}
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="pagination">
      <button class="btn btn-primary mx-2 text-white" onclick="paginate(this);" id="btnPrev"><svg viewBox="0 0 24 24" width="24" height="24" stroke="#ffff" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg> Back</button>
      <button class="btn btn-primary mx-2 text-white" onclick="paginate(this);" id="btnNext">Next<svg viewBox="0 0 24 24" width="24" height="24" stroke="#ffff" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg></button>
    </div>
</div>
<!-- Button trigger modal -->
{{-- <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#detailModalWebinar">
  Launch demo modal
</button> --}}

<!-- Modal -->
<div class="modal fade" id="detailModalWebinar" tabindex="-1" aria-labelledby="detailModalWebinarLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail Webinar</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Banner Webinar</label>
            <img src="" alt="" id="imageModalBanner" class="w-100">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Nama Webinar</label>
            <input disabled type="text" class="form-control" id="webinarName">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Tanggal Webinar</label>
            <input disabled type="text" class="form-control" id="tanggalWebinar">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Deskripsi Webinar</label>
            <p id="webinarDesc" class="form-control"></p>
            {{-- <textarea disabled cols="10" rows="10" class="form-control" height="20"  id="webinarDesc"></textarea> --}}
          </div>
          <div class="mb-3">
            <label for="">Link Registrasi</label>
            <div class="clipboard">
              <input type="text" class="w-100 clipboard-input"  id="webinarLinkRegist" disabled>
              <button class="clipboard-btn" id="clipboard-link-regist" data-button="" onclick="clipboard(this);">
                <svg viewBox="0 0 24 24" width="24" height="24" stroke="#D22344" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>
              </button>
            </div>
          </div>
          <div class="mb-3">
            <label for="">Link Monitoring</label>
            <div class="clipboard">
              <input type="text" class="w-100 clipboard-input" id="webinarLinkMonitoring" disabled>
              <button class="clipboard-btn" id="clipboard-link-monitoring" data-button="" onclick="clipboard(this);">
                <svg viewBox="0 0 24 24" width="24" height="24" stroke="#D22344" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg>
              </button>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-success" target="_blank" id="halamanMonitoring">Halaman Monitoring</a>
        <a type="button" class="btn btn-warning" target="_blank" id="halamanRegist">Halaman Registrasi</a>
        <a type="button" class="btn btn-primary" id="halamanEdit">Edit</a>
        <a type="button" class="btn btn-danger" onclick="deleteWebinar(this);" id="hapusWebinar">Hapus</a>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script>
  const getWebinarPaginate = async (endpoint) => {
    let btnPrev = $("#btnPrev");
    let btnNext = $("#btnNext");
    await fetch(endpoint)
          .then((response) => response.json())
          .then((data) => {
            let responseData = data.data;
            let datas = responseData.data;
            $("#containerWerbinar").html("");
            datas.forEach(val => {
                    let strRes = val.webinar_name.toUpperCase();
                    strRes = strRes.substr(0,40);
                    let template = `<div class="my-4 mx-2" id="webinar${val.id}">
                                      <div class="card" style="width: 18rem; height: 25rem">
                                        <img src="${val.banner_path}" style="height: 50%" class="card-img-top" alt="...">
                                        <div class="card-body">
                                          <h5 class="card-title">${val.webinar_name.length > 30 ? `${strRes}...` : strRes }</h5>
                                          <div class="" style="position: absolute;bottom: 0;left: 0;width: 100%;display: flex;justify-content: center;padding: 0 0.5rem;">
                                            <a href="javascript:void(0)" onclick="openModalWebinaar('${val.banner_path}','${val.webinar_name}','${btoa(val.deskripsi)}','${val.link_regist}','${val.link_monitoring}','${null}','${btoa(val.id)}','${val.be_held}');" class="btn btn-primary col mx-1">Detail</a>
                                            <span class="btn border border-primary text-primary visitors-input">
                                              <svg viewBox="0 0 24 24" width="20" height="20" stroke="#D22344" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><path d="M17 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="9" cy="7" r="4"></circle><path d="M23 21v-2a4 4 0 0 0-3-3.87"></path><path d="M16 3.13a4 4 0 0 1 0 7.75"></path></svg> &nbsp;: ${val.total_click}
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                                    </div>`
                    $("#containerWerbinar").append(template)
            });
            if(responseData.prev_page_url) {
              btnPrev.removeClass("disabled")
              btnPrev.attr("data-button", responseData.prev_page_url);
            } else {
              btnPrev.addClass("disabled")
            }
            if(responseData.next_page_url) {
              btnNext.removeClass("disabled")
              btnNext.attr("data-button", responseData.next_page_url);
            } else {
              btnNext.addClass("disabled")
            }
            console.log(responseData);
          })
  }
  getWebinarPaginate("{{route('admin.webinar.list')}}");

  const openModalWebinaar = (bannerUrl ,webinarName, descWebinar, linkRegist, linkMonitoring, linkEdit, idWebinar,dateWebinar) => {
    $("#imageModalBanner").attr("src", bannerUrl);
    $("#webinarName").val(webinarName);
    $("#webinarDesc").html(atob(descWebinar));
    $("#tanggalWebinar").val((dateWebinar));
    $("#webinarLinkRegist").val(linkRegist);
    $("#webinarLinkMonitoring").val(linkMonitoring);
    $("#halamanRegist").attr("href", linkRegist);
    $("#halamanMonitoring").attr("href", linkMonitoring);
    $("#halamanEdit").attr("href", `{{route('admin.webinar.edit', "")}}/${idWebinar}`);
    $("#hapusWebinar").attr("data-button", idWebinar);
    $("#clipboard-link-regist").attr("data-button",linkRegist);
    $("#clipboard-link-monitoring").attr("data-button",linkMonitoring);

    $("#detailModalWebinar").modal("show")
  }

  const paginate = async (self) => {
    let url = $(self).attr("data-button");
    await getWebinarPaginate(url);
  }
  
  const clipboard = (self) => {
    /* Get the text field */
    var copyText = document.getElementById("clipboard_input");
    let link = $("#clipboard_input").val($(self).attr("data-button"));
    console.log($(self).attr("data-button"));

    /* Select the text field */
    copyText.select();
    copyText.setSelectionRange(0, 99999); /* For mobile devices */

    /* Copy the text inside the text field */
    navigator.clipboard.writeText(copyText.value);

    /* Alert the copied text */
    Swal.fire({
        icon: 'success',
        title: "Pesan!",
        text: "Berhasil salin link: "+link.val(),
        showConfirmButton: false,
        timer: 2000
    });
 }

 const deleteWebinar = (self) => {
  let codeIdWebinar = $(self).attr("data-button");
  let url = "{{route('admin.webinar.delete')}}";
  Swal.fire({
      icon: 'warning',
      title: "Pesan!",
      text: "Apakah Anda ingin menghapus webinar ini?",
      showConfirmButton: true,
      showCancelButton: true,
      confirmButtonText: "IYA",
      cancelButtonText: "TIDAK",
  }).then(val => {
    if(val.isConfirmed) {
      $.ajax({
            url: url,
            dataType: "json",
            type: "DELETE",
            async: true,
            data: { codeIdWebinar },
            success: function (data) {
              Swal.fire({
                  icon: 'success',
                  title: "Pesan!",
                  text: data.message,
                  showConfirmButton: false,
                  timer: 2000
              });
              $("#webinar"+atob(codeIdWebinar)).remove();
              $("#detailModalWebinar").modal("hide")
            },
            error: function (xhr, exception) {
              let error = xhr.responseJSON;
              Swal.fire({
                  icon: 'error',
                  title: "Pesan!",
                  text: error.message,
                  showConfirmButton: false,
                  timer: 2000
              });
            }
      }); 
    }
  });
 }
</script>
@endsection