@extends("admin.template")

@section("title")
Ubah Password User
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item text-sm"><a class="opacity-5 text-white" href="javascript:;">Dashboard</a></li>
    <li class="breadcrumb-item text-sm text-white active" aria-current="page">Ganti Password Admin</li>    
@endsection

@section("content")
<div class="row">
    <div class="col-12">
      <div class="card mb-4">
        <div class="card-header pb-0">
          <h6>Ubah Password User</h6>
        </div>
        <div class="card-body px-0 pt-0 pb-2">
          <div class="p-4">
              <form id="formRestPassword">
                  <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Password Lama</label>
                    <input type="password" name="old_password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                  </div>
                  <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Password Baru</label>
                    <input type="password" name="new_password" class="form-control" id="exampleInputPassword1">
                  </div>
                  <div class="mb-3">
                      <label for="exampleInputPassword1" class="form-label">Konfirmasi Password Baru</label>
                      <input type="password" name="confirm_password" class="form-control" id="exampleInputPassword1">
                    </div>
                  <button type="button" class="btn btn-primary" onclick="resetPassword();">Simpan</button>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section("js")
<script>
  const resetPassword = () => {
    let datastring = $("#formRestPassword").serialize();
    let url = "{{route('admin.reset-password.admin.post')}}"
    $.ajax({
        url: url,
        dataType: "json",
        type: "Post",
        async: true,
        data: datastring,
        success: function (data) {
           if(data.status) {
            Swal.fire({
                icon: 'success',
                title: "Pesan!",
                text: data.message,
                showConfirmButton: false,
                timer: 3000
            });
            $("#formRestPassword input").val("");
           } else {
            Swal.fire({
                icon: 'error',
                title: "Pesan!",
                text: data.message,
                showConfirmButton: false,
                timer: 3000
            });
           }
        },
        error: function (xhr, exception) {
          console.log();
          let responseErr = xhr.responseJSON;
          Swal.fire({
                icon: 'error',
                title: "Pesan!",
                text: responseErr.message,
                showConfirmButton: false,
                timer: 3000
            });
        }
    }); 
  }
</script>
@endsection