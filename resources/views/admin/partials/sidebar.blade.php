<aside class="sidenav bg-white navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-4 " id="sidenav-main" style="z-index: 0">
    <div class="sidenav-header">
      <i class="fas fa-times p-3 cursor-pointer text-secondary opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
      <a class="navbar-brand m-0 text-center" href=" https://demos.creative-tim.com/argon-dashboard/pages/dashboard.html " target="_blank">
        <span class="ms-1 font-weight-bold text-center text-primary">Jendela Kerja Web Apps</span>
      </a>
    </div>
    <div class="collapse navbar-collapse  w-auto " id="sidenav-collapse-main">
      <ul class="navbar-nav">
        {{-- <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link " href="">
            
            <span class="text-center w-100 nav-link-text ms-1">Dashboard</span>
          </a>
        </li> --}}
        <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link {{url()->current() == route("admin.webinar.page") ? "active" : ""}}" href="{{route('admin.webinar.page')}}">
            
            <span class="text-center w-100 nav-link-text ms-1">Webinar</span>
          </a>
        </li>
        <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link {{url()->current() == route("admin.job.page") ? "active" : ""}}" href="{{route('admin.job.page')}}">
            
            <span class="text-center w-100 nav-link-text ms-1">Lowongan Kerja</span>
          </a>
        </li>
        {{-- <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link {{url()->current() == route("page.dashboard") ? "active" : ""}} " href="{{route("page.dashboard")}}">
            
            <span class="text-center w-100 nav-link-text ms-1">Dashboard</span>
          </a>
        </li>
        <p class="font-weight-bold  bg-gradient-danger text-white text-smalls text-center mb-1">ADMIN PAGES</p>
        <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link {{url()->current() == route("page.users") ? "active" : ""}}" href="{{route("page.users")}}">
            <span class="text-center w-100 nav-link-text  ms-1">Management Users</span>
          </a>
        </li>
        <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link {{url()->current() == route("role.page") ? "active" : ""}}" href="{{route("role.page")}}">
            <span class="text-center w-100 nav-link-text  ms-1">User Role</span>
          </a>
        </li>
        <p class="font-weight-bold  bg-gradient-danger text-white text-smalls text-center mb-1">DEVELOPER PAGES</p>
        <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link {{url()->current() == route("page.tokenize") ? "active" : ""}}" href="{{route("page.tokenize")}}">
            <span class="text-center w-100 nav-link-text  ms-1">Token Public API</span>
          </a>
        </li> --}}
        {{-- <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link " href="../pages/billing.html">
           
            <span class="text-center w-100 nav-link-text ms-1">Billing</span>
          </a>
        </li> --}}
        {{-- <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link " href="../pages/virtual-reality.html">
            
            <span class="text-center w-100 nav-link-text ms-1">Virtual Reality</span>
          </a>
        </li> --}}
        {{-- <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link " href="../pages/rtl.html">
            
            <span class="text-center w-100 nav-link-text ms-1">RTL</span>
          </a>
        </li> --}}
        {{-- <li class="nav-item d-flex justify-content-center mt-3">
          <h6 class="ps-4 ms-2 text-uppercase text-xs font-weight-bolder opacity-6">Account pages</h6>
        </li> --}}
        {{-- <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link " href="../pages/profile.html">
            
            <span class="text-center w-100 nav-link-text ms-1">Profile</span>
          </a>
        </li> --}}
        {{-- <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link " href="../pages/sign-in.html">
           
            <span class="text-center w-100 nav-link-text ms-1">Sign In</span>
          </a>
        </li> --}}
        {{-- <li class="nav-item d-flex justify-content-center">
          <a class="text-center w-100 nav-link " href="../pages/sign-up.html">
            
            <span class="text-center w-100 nav-link-text ms-1">Sign Up</span>
          </a>
        </li> --}}
      </ul>
    </div>
  </aside>