@extends('admin.template')

@section("title")
Edit Webinar
@endsection

@section('css')
<link rel="stylesheet" href="{{asset("css/filepond.css")}}"/>
<link rel="stylesheet" href="{{asset("css/filepond-image.css")}}"/>
<link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
<style>
  .gj-picker-md [role=header] {
    color: rgba(255,255,255,.54);
    display: flex;
    background: #d22444;
    align-items: baseline;
    user-select: none;
    justify-content: center;
}

.gj-picker-md div {
  font-size: 1.2rem!important;
}

.gj-picker-md [role=switch] {
    color: rgba(255,255,255,.54);
    background: #d22444;
    font-size: 32px;
}
</style>
@endsection


@section('breadcrumb')
<li class="breadcrumb-item text-sm"><a class="opacity-5 text-white" href="javascript:;">Dashboard</a></li>
<li class="breadcrumb-item text-sm text-white active" aria-current="page">Edit Webinar</li>    
@endsection

@section('content')
<div class="card-header pb-0">
  <h6>EDIT WEBINAR : <span class="font-weight-bold">{{$findWebinar->webinar_name}}</span></h6>
  
</div>
    <div class="card-body px-0 pt-0 pb-2" class="p-4">    
      <form class="p-4" enctype="multipart/form-data">
          <div class="mb-3">
            <label for="" class="">Upload Banner Webinar <span class="text-muted font-italic">(Jika ingin ganti banner silahkan upload banner lagi)</span></label>
            <input type="file" id="file_banner">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Nama Webinar</label>
            <input type="text" name="webinar_name" value="{{$findWebinar->webinar_name}}" class="form-control">
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Tanggal Diselenggarakan</label>
    let date = $("#datepicker").val();
            <input id="datepicker" class="form-control w-100" width="100%" value="{{date("d F Y H:i", strtotime($findWebinar->be_held))}}" />
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label">Deskripsi Webinar</label>
            <textarea name="desc" id="" cols="20" rows="10" class="form-control">{{$findWebinar->deskripsi}}</textarea>
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Link Pendaftaran</label>
            <input type="text" value="{{$findWebinar->link_regist}}"  name="link_regist" class="form-control">
          </div>
          <div class="mb-3">
            <label for="exampleInputPassword1" class="form-label">Link Monitoring</label>
            <input type="text" value="{{$findWebinar->link_monitoring}}" name="link_monitoring" class="form-control">
          </div>
          <button type="button" class="btn btn-primary" onclick="updateWebinar();">Simpan</button>
        </form>
  </div>
</div>
@endsection

@section('js')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="{{asset("js/filepond.min.js")}}"></script>
<script src="{{asset("js/filepond-image.js")}}"></script>
<script src="{{asset("js/filepond-jquery.js")}}"></script>
<script src="{{asset("js/filepond-encoded.js")}}"></script>
<script src="{{asset("js/filepond-validation-image.js")}}"></script>
<script src="https://cdn.jsdelivr.net/npm/@tinymce/tinymce-jquery@2/dist/tinymce-jquery.min.js"></script>
<script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/6/tinymce.min.js" referrerpolicy="origin"></script>
<script>
   var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
  $('#datepicker').datetimepicker({
      minDate: today,
      format: 'dd mmmm yyyy HH:MM',
      uiLibrary: 'bootstrap5',
      footer: true, modal: true
  });
  $('[name="desc"]').tinymce({ height: 500});

  FilePond.registerPlugin(FilePondPluginFileValidateType);
  FilePond.registerPlugin(FilePondPluginImagePreview);
  $('#file_banner').filepond();
  // Turn input element into a pond with configuration options
  $('#file_banner').filepond({
      allowMultiple: false,
      allowImagePreview: true,
      allowFileTypeValidation: true,
      allowProcess: true,
      allowFileEncode: true,
  });
  const filepond = FilePond.create(document.querySelector("#file_banner"), {
    onprocessfile: (err, fileItem) => {
      console.log(err, fileItem.file);
    },
    instantUpload: true,
    acceptedFileTypes: ['image/png','image/jpg','image/jpeg'],
    labelFileTypeNotAllowed: "File yang di bolehkan hanya .jpg/.png/.jpeg",

    fileValidateTypeDetectType: (source, type) =>
        new Promise((resolve, reject) => {
            // Do custom type detection here and return with promise
            resolve(type);
        }),
  });
  
  const updateWebinar = () => {
    let dataForm = new FormData();
    let webinarName = $("[name='webinar_name']").val();
    let desc = $("[name='desc']").val();
    let linkRegist = $("[name='link_regist']").val();
    let linkMonitoring = $("[name='link_monitoring']").val();
    console.log(desc, webinarName, linkRegist, linkMonitoring);
    let fileBanner = filepond.getFile();
    let date = $("#datepicker").val();
    dataForm.append("webinarName", webinarName == undefined ? "" : webinarName );
    dataForm.append("desc", desc == undefined ? "" : desc );
    dataForm.append("linkRegist", linkRegist == undefined ? "" : linkRegist );
    dataForm.append("linkMonitoring", linkMonitoring == undefined ? "" : linkMonitoring );
    dataForm.append("codeIdWebinar", "{{base64_encode($findWebinar->id)}}");
    dataForm.append("date", date == undefined ? "" : date );
    try {
      dataForm.append("fileBanner", fileBanner.file);  
    } catch (error) {
      
    }
    $.ajax({
        url: "{{ route('admin.create.webinar.edit.post') }}",
        dataType: "json",
        type: "POST",
        async: true,
        processData: false,
        contentType: false,
        data: dataForm,
        success: function(data) {
          Swal.fire({
              icon: 'success',
              title: "Pesan!",
              text: data.message,
              showConfirmButton: false,
              timer: 3000
          });
          setTimeout(() => {
            location.href = "{{route('admin.webinar.page')}}";
          }, 3000);
        },
        error: function(xhr, exception) {
          let error = xhr.responseJSON;
          Swal.fire({
              icon: 'error',
              title: "Pesan!",
              text: error.message,
              showConfirmButton: false,
              timer: 3000
          });
        }
    });

  }
</script>
@endsection