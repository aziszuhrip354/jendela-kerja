<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Jendela Kerja</title>
    <link
      href="{{asset('css/bootstrap5-2.css')}}"
      rel="stylesheet"
    />
    <link rel="stylesheet" href="{{asset('assets/css/styles.css')}}" />
    <link href="{{asset("css/swal.css")}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset("css/style.css")}}">

    <style>
      .container-loading {
        position: absolute;
        top: 0;
        display: flex;
        justify-content: center;
        align-items: center;
        background-color: #ffffffc4;
        left: 0;
        z-index: 2;
        height: 70vh;
        border-radius: 0.5rem;
      }

      .line {
          display: inline-block;
          width: 30px;
          height: 70px;
          border-radius: 40px;
          margin: 0 0.5rem;
          background-color: #D22344;
          box-shadow: none;
      }

      
    </style>
  </head>
  <body>
    <header>
      <!-- NAVBAR -->
      <nav class="navbar navbar-expand-lg">
        <div class="container navbar-content">
          <a class="navbar-brand" href="#beranda">Carexpo</a>
          <button class="border-none btnHamburger" id="btnHamburger">
          <svg viewBox="0 0 24 24" width="24" height="24" stroke="currentColor" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>
          </button>
          <div class="collapse navbar-collapse" id="navbarSupportedContent" data-button="false">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0" id="navbarMobile">
              <li class="nav-item">
                <a class="nav-link" href="#beranda">Beranda</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#webinar">Webinar</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#tentang">Tentang</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#loker">Loker</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#kontak">Kontak</a>
              </li>
              @if(!session("verify_as"))
                <li class="nav-item">
                  <a class="nav-link" href="{{route('signIn')}}">Masuk</a>
                </li>
              @else
                <li class="nav-item">
                  <a class="nav-link" href="javascript:void(0)" onclick="logOut('logoutForm')">Logout</a>
                  <form action="{{route('logout')}}" id="logoutForm" method="POST">
                      @csrf
                  </form>
                </li>
              @endif
            </ul>
            <div class="text-center t-small mt-3 d-none footer-navbar-mobile">
              © 2020 Tickitz. All Rights Reserved.
            </div>
            <div class="d-flex d-none-in-mobile" role="search">
                @if (!session("verify_as"))
                    <a class="btn-white" href="{{route('signIn')}}">Masuk</a>
                @else
                    <a class="btn-white" href="javascript:void(0)" onclick="logOut('logoutForm')">Logout</a>
                    <form action="{{route('logout')}}" id="logoutForm" method="POST">
                        @csrf
                    </form>
                @endif
            </div>
          </div>
        </div>
      </nav>

      <!-- HERO -->
      <section class="hero" id="beranda">
        <div class="hero-content container">
          <div class="row">
            <div class="col">
              <div class="hero-left text-break">
                <h1>
                  Selamat datang di
                  <div class="red">Jendela Kerja!</div>
                </h1>
                <p>
                  Jendela Kerja merupakan salah satu mata acara dari Carexpo 2022. Jendela Kerja menyediakan informasi terkait lowongan pekerjaan, lowongan magang, serta webinar kepada mahasiswa aktif dan fresh graduate FIB UI. Jendela Kerja akan menjadi jembatan bagi mahasiswa aktif dan fresh graduate FIB UI untuk melangkah ke jenjang karir.
                </p>
                <a class="btn-red" id="btnDetail" href="">Lihat lebih lanjut!</a>
              </div>
            </div>
            <div class="col d-flex align-items-center">
              <div class="hero-right">
                <img src="{{asset("assets/images/hero-img.png")}}" alt="" />
              </div>
            </div>
          </div>
        </div>
        <div class="hero-bg">
          <div class="hero-bg-left"></div>
          <div class="hero-bg-right"></div>
        </div>
      </section>
    </header>

    <main>
      <!-- WEBINAR -->
      <section class="webinar container" id="webinar">
        <div class="section-title">
          <h2>Jadwal <span class="red">Webinar</span></h2>
        </div>
        <div class="webinar-content" id="webinarContainer">
          {{-- <div class="webinar-card">
            <img src="{{asset("assets/images/img-webinar.png")}}" alt="" />
            <h3>Nama Webinar</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh
              vitae tincidunt fringilla commodo gravida.
            </p>
            <button class="btn-red-detail">Daftar</button>
          </div>
          <div class="webinar-card">
            <img src="{{asset("assets/images/img-webinar.png")}}" alt="" />
            <h3>Nama Webinar</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh
              vitae tincidunt fringilla commodo gravida.
            </p>
            <button class="btn-red-detail">Daftar</button>
          </div>
          <div class="webinar-card">
            <img src="{{asset("assets/images/img-webinar.png")}}" alt="" />
            <h3>Nama Webinar</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh
              vitae tincidunt fringilla commodo gravida.
            </p>
            <button class="btn-red-detail">Daftar</button>
          </div>
          <div class="webinar-card">
            <img src="{{asset("assets/images/img-webinar.png")}}" alt="" />
            <h3>Nama Webinar</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh
              vitae tincidunt fringilla commodo gravida.
            </p>
            <button class="btn-red-detail">Daftar</button>
          </div> --}}
        </div>
      </section>

      <!-- TENTANG -->
      <section class="tentang container" id="tentang">
        <div class="tentang-video">
          <iframe
            width="560"
            height="315"
            src="https://www.youtube.com/embed/Dap9zXOQSnE"
            title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen
          ></iframe>
        </div>
        <div class="tentang-title">
          <h2>
            Tentang
            <div class="red">Jendela Kerja</div>
          </h2>
          <p>
            Career Expo atau Carexpo adalah salah satu program kerja Departemen Pengembangan Karier BEM FIB UI 2022 yang menyajikan informasi seputar dunia kerja baik melalui website maupun rangkaian seminar. Carexpo terdiri dari dua mata acara, yakni TMI Senja (Tempat Menerima Informasi Seputar Dunia Kerja) dan Jender (Jendela Kerja).
          </p>
        </div>
      </section>

      <!-- LOKER -->
      <div class="loker container">
        <div class="section-title">
          <h2>Lowongan <span class="red">Pekerjaan</span></h2>
        </div>
        <div class="row mb-4">
          <div class="col">
            <input type="text" onkeyup="searchingJob();" id="searchJob" class="w-100" placeholder="Cari posisi / nama perusahaan" style="    padding: 0.6rem 2rem;border-radius: 0.3rem;border: none;background-color: #EDF5F8;padding-right: 0.5rem;">
          </div>
          <div class="col"></div>
          <div class="col d-flex justify-content-end">
            <select name="" onchange="searchingJob();" id="filterJob" style="border: none;background-color: #ecf5f8;text-align: center;width: 180px;border-radius: 0.5rem;">
              <option value="default">Filter</option>
              <option value="latest">Terbaru</option>
              <option value="oldest">Terlama</option>
              <option value="most_like">Peminat Tinggi</option>
              <option value="not_most_like">Peminat Rendah</option>
            </select>
          </div>
        </div>
        <section class="container" id="loker" >
          <div style="position: relative">
            <div class="container-loading vh-100 w-100" id="containerLoading">
              <div class="load-wrapp">
                  <div class="load-3 flexs" style="    margin-bottom: 8rem;">
                    <h4 class="fw-bold text-center text-danger">Loading ...</h4>
                    <div class="d-flex justify-content-center">
                        <div class="line"></div>
                        <div class="line"></div>
                        <div class="line"></div>
                    </div>
                  </div>
              </div>
            </div>
            <div class="row" id="containerJob" style="height: 70vh;">
              {{-- <div class="col-4">
                <div class="loker-card">
                  <div class="d-flex">
                    <img src="{{asset("assets/images/logo-perusahaan.png")}}" alt="" />
                    <div>
                      <h3>Posisi Pekerjaan</h3>
                      <h4>Nama Perusahaan</h4>
                    </div>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh
                    vitae tincidunt fringilla commodo gravida.
                  </p>
                  <button class="btn-red-loker mb-3">Daftar</button>
                  <button class="btn-transparent-loker">Lihat Poster</button>
                </div>
              </div>
              <div class="col-4">
                <div class="loker-card">
                  <div class="d-flex">
                    <img src="{{asset("assets/images/logo-perusahaan.png")}}" alt="" />
                    <div>
                      <h3>Posisi Pekerjaan</h3>
                      <h4>Nama Perusahaan</h4>
                    </div>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh
                    vitae tincidunt fringilla commodo gravida.
                  </p>
                  <button class="btn-red-loker mb-3">Daftar</button>
                  <button class="btn-transparent-loker">Lihat Poster</button>
                </div>
              </div>
              <div class="col-4">
                <div class="loker-card">
                  <div class="d-flex">
                    <img src="{{asset("assets/images/logo-perusahaan.png")}}" alt="" />
                    <div>
                      <h3>Posisi Pekerjaan</h3>
                      <h4>Nama Perusahaan</h4>
                    </div>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh
                    vitae tincidunt fringilla commodo gravida.
                  </p>
                  <button class="btn-red-loker mb-3">Daftar</button>
                  <button class="btn-transparent-loker">Lihat Poster</button>
                </div>
              </div>
              <div class="col-4">
                <div class="loker-card">
                  <div class="d-flex">
                    <img src="{{asset("assets/images/logo-perusahaan.png")}}" alt="" />
                    <div>
                      <h3>Posisi Pekerjaan</h3>
                      <h4>Nama Perusahaan</h4>
                    </div>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh
                    vitae tincidunt fringilla commodo gravida.
                  </p>
                  <button class="btn-red-loker mb-3">Daftar</button>
                  <button class="btn-transparent-loker">Lihat Poster</button>
                </div>
              </div>
              <div class="col-4">
                <div class="loker-card">
                  <div class="d-flex">
                    <img src="{{asset("assets/images/logo-perusahaan.png")}}" alt="" />
                    <div>
                      <h3>Posisi Pekerjaan</h3>
                      <h4>Nama Perusahaan</h4>
                    </div>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nibh
                    vitae tincidunt fringilla commodo gravida.
                  </p>
                  <button class="btn-red-loker mb-3">Daftar</button>
                  <button class="btn-transparent-loker">Lihat Poster</button>
                </div>
              </div> --}}
            </div>
          </div>
        </section>
        <div class="container my-4">
          <div class="d-flex justify-content-center" style="    padding: 3rem 0;"  id="containerPage">
            {{-- <button class="btn bg-transparent-loker-gray mx-2 disabled">
                <svg viewBox="0 0 24 24" width="24" height="24" stroke="#555064" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="15 18 9 12 15 6"></polyline></svg>
            </button>
            <button class="btn bg-transparent-loker-gray mx-2">1</button>
            <button class="btn bg-transparent-loker-gray mx-2">2</button>
            <button class="btn bg-transparent-loker-gray mx-2">3</button>
            <button class="btn bg-transparent-loker-gray mx-2">
              <svg viewBox="0 0 24 24" width="24" height="24" stroke="#555064" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="9 18 15 12 9 6"></polyline></svg>
            </button> --}}
          </div>
        </div>
      </div>
    </main>

    <footer>
      <div class="footer-content container" id="kontak">
        <div class="desc">
          <h2 class="t-left mb-2">Jendela Kerja</h2>
          <p class="t-left">
            Career Expo FIB UI 2022
          </p>
        </div>
        <div class="informasi t-left">
          <h2 class="t-left mb-2">Informasi Kontak</h2>
          <div>
            <div class="d-flex foot-list">
              <img src="{{asset("assets/images/icon-location.svg")}}" alt="" />
              <a href="" target="_blank"
                >Pondok Cina, Beji, Depok City, West Java 16424</a
              >
            </div>
            <div class="d-flex foot-list">
              <img src="{{asset("assets/images/icon-tele.svg")}}" alt="" />
              <a href="https://wa.me/+6281310524374" target="_blank">+62 813-1052-4374</a>
            </div>
            <div class="d-flex foot-list">
              <img src="{{asset("assets/images/icon-email.svg")}}" alt="" />
              <a href="" target="_blank">jendela.kerja@gmail.com</a>
            </div>
          </div>
        </div>
        <div class="sosmed">
          <h2 class="t-left mb-2">Social Media</h2>
          <div>
            {{-- <div class="d-flex foot-list">
              <img src="{{asset("assets/images/icon-facebook.svg")}}" alt="" />
              <a href="" target="_blank">Jendela Kerja</a>
            </div> --}}
            <div class="d-flex foot-list">
              <img src="{{asset("assets/images/icon-ig.svg")}}" alt="" />
              <a href="" target="_blank">@careerexpofibu</a>
            </div>
            {{-- <div class="d-flex foot-list">
              <img src="{{asset("assets/images/icon-twitter.svg")}}" alt="" />
              <a href="" target="_blank">jendela.kerja</a>
            </div>
            <div class="d-flex foot-list">
              <img src="{{asset("assets/images/icon-yt.svg")}}" alt="" />
              <a href="" target="_blank">Jendela Kerja</a>
            </div> --}}
          </div>
        </div>
      </div>
      <div class="copyright container">
        <div class="copyright-line"></div>
        <p>© 2022 Jendela Kerja. All rights reserved</p>
      </div>
    </footer>
    <!-- Button trigger modal -->

<!-- Modal -->
<div class="modal fade" id="modalPoster" tabindex="-1" role="dialog" aria-labelledby="modalPosterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="my-3">
        <div>
          <h5 class="modal-title txt-primary  fw-bold text-center" id="titlePoster1">Modal title</h5>
          <h6 class="modal-title txt-secondary fw-bold text-center" id="titlePoster2">Modal title</h5>
        </div>
      </div>
      <div class="modal-body">
        <img src="" class="w-100" alt="" id="imgPoster">
      </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalDeskripsiJob" tabindex="-1" role="dialog" aria-labelledby="modalDeskripsiJobTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="my-3">
        <div>
          <h5 class="modal-title txt-secondary  fw-bold text-center" id="titlePoster1">Detail Deskripsi Lowongan</h5>
        </div>
      </div>
      <div class="modal-body">
        <p class="txt-primary  fw-bold" id="detailJob"></p>
      </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalDetailWebinar" tabindex="-1" role="dialog" aria-labelledby="modalDetailWebinarTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
    <div class="modal-content">
      <div class="my-3">
        <div>
          <h5 class="modal-title txt-primary  fw-bold text-center" id="tilteWebinar">Detail Webinar</h5>
        </div>
      </div>
      <div class="modal-body">
        <form>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label txt-secondary  fw-bold">Judul Webinar</label>
            <p class="txt-primary fw-bold" id="judulWebinar">Test</p>
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label txt-secondary  fw-bold">Tanggal</label>
            <p class="txt-primary fw-bold" id="tanggalWebinar">Test</p>
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1" class="form-label txt-secondary  fw-bold">Deskripsi</label>
            <p class="txt-primary fw-bold" id="deskripsiWebinar">Test</p>
          </div>
          <div class="mb-3">
            <label for="exampleInputEmail1"  class="form-label txt-secondary  fw-bold">Poster</label>
            <img src="" class="w-100" alt="" id="posterWebinar">
          </div>
        </form>
      </div>
      </div>
    </div>
  </div>
</div>

    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa"
      crossorigin="anonymous"
    ></script>
    <script src="{{asset("js/jquery3-6.js")}}"></script>
    <script src="{{asset("js/js-bootstrap5-2.js")}}"></script>
    <script src="{{asset("js/swal2.js")}}"></script>
    <script>
    $("#btnHamburger").on("click", function() {
      let dataButton = $("#navbarSupportedContent").attr("data-button");
      if(dataButton == "false") {
        $("#navbarSupportedContent").attr("data-button", "true")
        $("#navbarSupportedContent").addClass("show");
      } else {
        $("#navbarSupportedContent").attr("data-button", "false")
        $("#navbarSupportedContent").removeClass("show");
      }
    });
    @if(session("error"))
        Swal.fire({
            icon: 'error',
            title: "Pesan!",
            text: "{{session('error')}}",
            showConfirmButton: false,
            timer: 3000
        });
    @endif

    var $loading = $('#containerLoading').hide();

      const nav = document.querySelector("nav");

      window.addEventListener("scroll", () => {
        if (window.scrollY >= 50) {
          nav.classList.add("active_nav");
        } else {
          nav.classList.remove("active_nav");
        }
      });

      const logOut = (idForm) => {
        let form = document.getElementById(idForm);
        form.submit();
    }

const openDetailWebinar = (title, deskripsi, src,date) => {
  $("#modalDetailWebinar").modal("show");
  $("#judulWebinar").text(title)
  $("#deskripsiWebinar").html(atob(deskripsi))
  $("#posterWebinar").attr("src", src)
  $("#tanggalWebinar").text(date)
}

const getWebinarPaginate = async (endpoint) => {
    await fetch(endpoint)
          .then((response) => response.json())
          .then((data) => {
            console.log(data);
            let datas = data.data;
            $("#webinarContainer").html("");
            if(datas.length < 0) {
                // $("#webinarContainer").remove();
                let notFoundJob = `<div style="display: flex;flex-direction: column;align-items: center;justify-content: center;">
                                    <svg viewBox="0 0 24 24" width="400" height="400" stroke="#D22344" stroke-width="1" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="9" y1="9" x2="15" y2="15"></line><line x1="15" y1="9" x2="9" y2="15"></line></svg>
                                    <h4 style="font-weight:bold" class="text-danger">WEBINAR KOSONG</h4>
                                </div>`;
                $("#webinarContainer").html(notFoundJob);
                return
            }
            datas.forEach(val => {
                    let strRes = val.deskripsi.toUpperCase();
                    let strRes2 = val.webinar_name.toUpperCase();
                    strRes = strRes.substr(0,40);
                    strRes = strRes2.substr(0,20);
                    @if(session("verify_as")!=null)
                      let template = `<div class="webinar-card" onclick="openDetailWebinar('${val.webinar_name}','${btoa(val.deskripsi)}','${val.banner_path}','${val.be_held}');" style="position: relative">
                                          <img src="${val.banner_path}" class="w-100" alt="" / style="height:50%">
                                          <h3>${val.webinar_name.length > 20 ? strRes2+"...." : strRes2}</h3>
                                          <p>
                                          ${val.deskripsi.length > 40 ? strRes+"...." : strRes}
                                          </p>
                                          <a href="{{route('link.regist',"")}}/${btoa(val.id)}" target="_blank" class="w-100 py-2 btn btn-danger" style="position: absolute;left: 30px;width: 80%!important;bottom: 25px;">Daftar</a>
                                      </div>`
                    @else
                       let template = `<div class="webinar-card" onclick="openDetailWebinar('${val.webinar_name}','${btoa(val.deskripsi)}','${val.banner_path}','${val.be_held}');" style="position: relative">
                                          <img src="${val.banner_path}" class="w-100" alt="" / style="height:50%">
                                          <h3>${val.webinar_name.length > 20 ? strRes2+"...." : strRes2}</h3>
                                          <p>
                                          ${val.deskripsi.length > 40 ? strRes+"...." : strRes}
                                          </p>
                                          <button onclick="openDetailWebinar('${val.webinar_name}','${btoa(val.deskripsi)}','${val.banner_path}');" class="w-100 py-2 btn btn-danger" style="position: absolute;left: 30px;width: 80%!important;bottom: 25px;">Detail</button>
                                      </div>`
                    @endif
                    $("#webinarContainer").append(template)
            });
          })
}
let skipped = 0;
let limit = 5;
getWebinarPaginate(`{{route('landingpage.list',["",""])}}/${skipped}/${limit}`)

const getJobPaginate = async (endpoint) => {
    $loading.show();
    await fetch(endpoint)
          .then((response) => response.json())
          .then((data) => {
            console.log(data);
            let responseData = data.data;
            let datas = responseData.data;
            let links = responseData.links;
            $("#containerJob").html("");
            $("#containerPage").html("");
            if(datas.length == 0) {
              let notFoundJob = `<div style="display: flex;flex-direction: column;align-items: center;justify-content: center;">
                                    <svg viewBox="0 0 24 24" width="400" height="400" stroke="#D22344" stroke-width="1" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><rect x="3" y="3" width="18" height="18" rx="2" ry="2"></rect><line x1="9" y1="9" x2="15" y2="15"></line><line x1="15" y1="9" x2="9" y2="15"></line></svg>
                                    <h4 style="font-weight:bold" class="text-danger">LOKER KOSONG</h4>
                                </div>`;
              $("#containerJob").html(notFoundJob);
            }
            datas.forEach(val => {
                    var strippedHtml = val.job_desc.replace(/<[^>]+>/g, '');
                    strippedHtml = strippedHtml.replace("&nbsp;","");
                    strippedHtml = strippedHtml.replace("&NBSP;","");
                    let strRes = strippedHtml.toUpperCase();
                    strRes = strRes.substr(0,70);
                    console.log(strRes.length);
                    let template = `<div class="col-4 listJobs" >
                                      <div class="loker-card">
                                        <div class="d-flex">
                                          <img src="${val.icon_path}" alt="" />
                                          <div>
                                            <h3>${val.position}</h3>
                                            <h4>${val.company_name}</h4>
                                          </div>
                                        </div>
                                        <p>
                                          ${val.job_desc.length > 70 ? `${strRes}...<a href="javascript:void(0)" onclick="openDetailDeskripsiJob('${btoa(val.job_desc)}');" style="    font-style: italic;
    font-size: 0.8rem;">Selengkapnya</a>` : strRes}
                                        </p>
                                        <div class="row" style="position:absolute;width: 100%;right: 10px;bottom: 15px;">
                                          <a class="w-100 btn-danger col" href="{{route('link.regist.job','')}}/${btoa(val.id)}" target="_blank">
                                            <button class="w-100 btn-red-loker .bg-transparent-loker">Daftar</button>  
                                          </a>
                                          <a class="w-100 btn-danger col" href="javascript:void(0)" onclick="openPoster('${val.poster_path}','${val.position}','${val.company_name}')">
                                            <button class="w-100 btn-red-loker bg-transparent-loker">Lihat Poster</button>  
                                          </a>
                                        </div>
                                      </div>
                                    </div>`
                              
                    $("#containerJob").append(template)
            });
            let counter = 0;
            let iconFirstPage = `<svg viewBox="0 0 24 24" width="24" height="24" stroke="#555064" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="15 18 9 12 15 6"></polyline></svg>`;
            let iconLastPage = `<svg viewBox="0 0 24 24" width="24" height="24" stroke="#555064" stroke-width="2" fill="none" stroke-linecap="round" stroke-linejoin="round" class="css-i6dzq1"><polyline points="9 18 15 12 9 6"></polyline></svg>`;
            let lastPage = links.length - 1;
            if(datas.length != 0) {
              let currPage = responseData.current_page;
              console.log(currPage);
              links.forEach(val => {
                if(parseInt(val.label) >= currPage - 3 && parseInt(val.label) <= (currPage+3)) {
                  let el = `<button onclick="paginate(this);" class="${val.url != null ? "" : "disabled"} ${val.active ? "btn-pagination-job" : ""} btn bg-transparent-loker-gray mx-2" data-button="${val.url}" style="    width: 50px;">
                              ${counter == 0 ? iconFirstPage:""}
                              ${counter == lastPage ? iconLastPage:""}
                              ${counter != 0 && counter != lastPage ? val.label:""}
                            </button>`;
                  $("#containerPage").append(el)
                }
                counter++;
              });
              $("#containerJob").delay(500).fadeIn(500);
            }
          })
    setTimeout(() => {
      $loading.hide();
    }, 500);
}
getJobPaginate('{{route('job.pagination')}}')

const searchingJob = () => {
  let searchJob = $("#searchJob").val();
  let filterJob = $("#filterJob").val();
  if(searchJob != "" && filterJob != "") {
    getJobPaginate(`{{route('job.pagination')}}/?search=${searchJob}&filter=${filterJob}`);
  } else if (filterJob != "default") {
    getJobPaginate(`{{route('job.pagination')}}/?filter=${filterJob}`)
  } else {
    getJobPaginate(`{{route('job.pagination')}}/?search=${searchJob}`)
  }
}

const paginate = async (self) => {
    let url = $(self).attr("data-button");
    await getJobPaginate(url);
}

const openPoster = (imgSrc, position, company) => {
  $("#modalPoster").modal("show");
  $("#titlePoster1").text(`${company}`)
  $("#titlePoster2").text(`${position}`)
  $("#imgPoster").attr("src", `${imgSrc}`);
}

const openDetailDeskripsiJob = (desc) => {
  $("#modalDeskripsiJob").modal("show")
  $("#detailJob").html(atob(desc))
}


    </script>
  </body>
</html>
