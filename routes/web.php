<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\LandingpageController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route("landingpage");
});

Route::get("/landing-page", [LandingpageController::class,"landingpage"])->name("landingpage");
Route::get("/get-webinar/{skipped}/{limit}", [LandingpageController::class,"getDataWebinar"])->name("landingpage.list");
Route::get("/get-job-pagination", [LandingpageController::class,"getJobPagination"])->name("job.pagination");

Route::group(["middleware" => "auth.password"], function() {
    Route::get("/link-webinar/{codeId}", [LandingpageController::class,"linkRegist"])->name("link.regist");
    Route::get("/link-regist-job/{codeId}", [LandingpageController::class,"linkRegistJob"])->name("link.regist.job");
    Route::group(["prefix" => "admin-site"],function() {
        Route::controller(AdminController::class)->group(function() { 
            Route::group(["middleware" => "auth.admin"], function() {
                Route::get("page-webinar","webinarPage")->name("admin.webinar.page");
                Route::get("edit-webinar/{code_webinar}","webinarEditPage")->name("admin.webinar.edit");
                Route::get("list-webinar", "listWebinar")->name("admin.webinar.list");
                Route::delete("hapus-webinar", "deleteWebinar")->name("admin.webinar.delete");
                Route::post("edit-webinar", "updateWebinar")->name("admin.create.webinar.edit.post");
                
                Route::get("buat-webinar","createWebinarPage")->name("admin.create.webinar.page");
                Route::post("buat-webinar","postWebinar")->name("admin.create.webinar.post");

                Route::get("page-job","jobPage")->name("admin.job.page");
                Route::get("edit-job/{code_job}","jobEditPage")->name("admin.job.edit");
                Route::get("list-job", "listJob")->name("admin.job.list");
                Route::delete("hapus-job", "deleteJob")->name("admin.job.delete");
                Route::post("edit-job", "updateJob")->name("admin.create.job.edit.post");
                
                Route::get("buat-job","createJobPage")->name("admin.create.job.page");
                Route::post("buat-job","postJob")->name("admin.create.job.post");
                
                
                Route::get("ubah-password-user","resetPasswordUser")->name("admin.reset-password.user");
                Route::post("ubah-password-user/post","resetPasswordUserPost")->name("admin.reset-password.user.post");

                Route::get("ubah-password-admin","resetPasswordAdmin")->name("admin.reset-password.admin");
                Route::post("ubah-password-admin/post","resetPasswordAdminPost")->name("admin.reset-password.admin.post");
            });
        });
    });

    Route::post("/logout",[AuthController::class,"logout"])->name("logout");
});

Route::controller(AuthController::class)->group(function () {
    Route::group(["prefix" => "auth"], function() {

        // SIGNIN USER
        Route::get("sign-in","signIn")->name("signIn");
        Route::post("send", "signInPost")->name("signIn.post");

        // SIGNIN ADMIN
        Route::get("sign-in/admin","sigInAdmin")->name("signIn.admin");
        Route::post("send/admin", "signInPost")->name("signIn.post.admin");
    });
});




